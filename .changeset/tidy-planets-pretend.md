---
'lint-staged': patch
---

Handle possible failures when logging user shell for debug info.
